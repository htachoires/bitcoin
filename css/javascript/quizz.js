(function () {
    var questions = [{
        question: '<a class="font-weight-bold">Question 1:</a> Haut responsable de la gestion et de l’analyse des données en masse (Big Data), je dois leur attribuer un sens et en extraire de la valeur pour aider à prendre des décisions. Malgré des formations spécialisées encore peu présentes dans mon domaine, je nécessite un bac +5, avec de bonnes compétences en informatique, management, statistiques ou en marketing. Recrutés par les industries, les grandes entreprises, les commerces, ou même des organisations médicales, je suis amené à encore me développer car mon domaine est un enjeu majeur pour les sociétés.',
        choices: [' Administrateur base de données', ' Web designer', ' Conseiller en e-réputation', ' Data scientist'],
        correctAnswer: 3
    }, {
        question: '<a class="font-weight-bold">Question 2:</a>' + " Logique, rigoureux, pragmatique, autonome et à l’écoute, je dois être capable de m’adapter rapidement. En suivant un cahier des charges des besoins des utilisateurs, j’envisage les solutions et les retranscris en lignes de code dans le langage informatique adéquat. L’informatique se développant de plus en plus dans la société actuelle, je peux me présenter dans beaucoup de secteurs de travail (mais particulièrement en web et en telecom). Un bac +2 est suffisant mais je peux me développer jusqu'à bac +5. De plus, je dois savoir analyser, concevoir, et programmer.",
        choices: [' Game designer', ' Développeur informatique', ' Administrateur réseaux', ' Dépanneur informatique'],
        correctAnswer: 1
    }, {
        question: '<a class="font-weight-bold">' + "Question 3:</a> Travaillant pour un client, je dois être à l'écoute afin de créer l'identité visuelle de son site internet. Je suis chargé de réaliser les éléments graphiques comme les illustrations, les bannières, les animations flash, les fonds d'écran et parfois même les logos. Une simple formation artistique suivie d'une formation informatique me permet d'exercer ce métier. Cependant, je peux également avoir BTS design graphique.",
        choices: [' Nettoyeur Web', " Responsable d'assistance informatique", ' Web designer', ' Expert en sécurité informatique'],
        correctAnswer: 2
    }];

    var questionCounter = 0; //Tracks question number
    var selections = []; //Array containing user choices
    var quiz = $('#quiz'); //Quiz div object

    // Display initial question
    displayNext();

    // Click handler for the 'next' button
    $('#next').on('click', function (e) {
        e.preventDefault();

        // Suspend click listener during fade animation
        if (quiz.is(':animated')) {
            return false;
        }
        choose();

        // If no user selection, progress is stopped
        if (isNaN(selections[questionCounter])) {
            alert('Une réponse doit être choisie !');
        } else {
            questionCounter++;
            displayNext();
        }
    });

    // Click handler for the 'prev' button
    $('#prev').on('click', function (e) {
        e.preventDefault();

        if (quiz.is(':animated')) {
            return false;
        }
        choose();
        questionCounter--;
        displayNext();
    });

    // Click handler for the 'Start Over' button
    $('#start').on('click', function (e) {
        e.preventDefault();

        if (quiz.is(':animated')) {
            return false;
        }
        questionCounter = 0;
        selections = [];
        displayNext();
        $('#start').hide();
    });

    // Animates buttons on hover
    $('.button').on('mouseenter', function () {
        $(this).addClass('active');
    });
    $('.button').on('mouseleave', function () {
        $(this).removeClass('active');
    });

    // Creates and returns the div that contains the questions and 
    // the answer selections
    function createQuestionElement(index) {
        var qElement = $('<div>', {
            id: 'question'
        });

        var question = $('<p>').append(questions[index].question);
        qElement.append(question);

        var radioButtons = createRadios(index);
        qElement.append(radioButtons);

        return qElement;
    }

    // Creates a list of the answer choices as radio inputs
    function createRadios(index) {
        var radioList = $('<ul class="list-group">');
        var item;
        var input = '';
        for (var i = 0; i < questions[index].choices.length; i++) {
            item = $('<li class="list-group-item">');
            input = '<input type="radio" name="answer" value=' + i + ' />';
            input += questions[index].choices[i];
            item.append(input);
            radioList.append(item);
        }
        return radioList;
    }

    // Reads the user selection and pushes the value to an array
    function choose() {
        selections[questionCounter] = +$('input[name="answer"]:checked').val();
    }

    // Displays next requested element
    function displayNext() {
        quiz.fadeOut(function () {
            $('#question').remove();

            if (questionCounter < questions.length) {
                var nextQuestion = createQuestionElement(questionCounter);
                quiz.append(nextQuestion).fadeIn();
                if (!(isNaN(selections[questionCounter]))) {
                    $('input[value=' + selections[questionCounter] + ']').prop('checked', true);
                }

                // Controls display of 'prev' button
                if (questionCounter === 1) {
                    $('#prev').show();
                } else if (questionCounter === 0) {

                    $('#prev').hide();
                    $('#next').show();
                }
            } else {
                var scoreElem = displayScore();
                quiz.append(scoreElem).fadeIn();
                $('#next').hide();
                $('#prev').hide();
                $('#start').show();
            }
        });
    }

    // Computes score and returns a paragraph element to be displayed
    function displayScore() {
        var score = $('<p>', { id: 'question' });

        var numCorrect = 0;
        for (var i = 0; i < selections.length; i++) {
            if (selections[i] === questions[i].correctAnswer) {
                numCorrect++;
            }
        }
        if (numCorrect == 3) {
            score.append('Bravo ! Toutes les réponses étaient correctes :)');
            return score;
        }
        if (numCorrect == 2) {
            score.append('Bien joué ! ' + numCorrect + ' réponses étaient correctes.');
            return score;
        }
        if (numCorrect == 1) {
            score.append(' ' + numCorrect + ' réponse sur ' +
                questions.length + " était correcte, tu peux recommencer pour t'améliorer.");
            return score;
        }
        if (numCorrect == 0) {
            score.append("Aucune réponse n'était correcte :( Tu peux recommencer pour t'améliorer.");
            return score;
        }
    }
})();